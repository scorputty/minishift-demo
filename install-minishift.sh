#!/usr/bin/env bash

function press (){
  read -n 1 -s -r -p "Press any key to continue"
}

clear

echo "Installing minishift"
press

echo ""

brew cask install --force minishift

echo ""

echo "Now run one of the profile setups scipts"

ls *setup*.sh
