# minishift-demo

Demo code for minishift

### Notes
After installing minishift you need to fix the DNS on OSX
``
Closing this as there are two workarounds to this issue (which is not minishift's anyway):

After minishift start, log into the VM with minishift ssh and change the nameserver x.x.x.x value in /etc/resolv.conf to a valid DNS server (as per my previous comment. Note you will have to do this everytime you stop/start the VM.
A more permanent fix is have a local dnsmasq listening on all interfaces (listen-address=0.0.0.0), so that the default DNS value for the VM can use your dnsmasq instance as a DNS server.
``

### extra trickery to get the istio demo working
Install helm
```sh
brew install kubernetes-helm
```
## Follow this howto:
- https://istio.io/docs/setup/kubernetes/install/helm/
Before the install step run the prep scrip:
```sh
./prep-os.sh
```
To add Kiali with all bells an whistles use this install script:
```sh
./stef-helm.#!/bin/sh
```
After install you can install the demo app:
- https://istio.io/docs/examples/bookinfo/
To het the ingress link do this:
```sh
❯ minishift ip
192.168.99.109
> kubectl get svc istio-ingressgateway -n istio-system
NAME                   TYPE           CLUSTER-IP       EXTERNAL-IP                   PORT(S)                                                                                                                                      AGE
istio-ingressgateway   LoadBalancer   172.30.148.152   172.29.72.211,172.29.72.211   80:31380/TCP,443:31390/TCP,31400:31400/TCP,15029:31050/TCP,15030:30949/TCP,15031:31225/TCP,15032:32299/TCP,15443:30837/TCP,15020:31054/TCP   1h
❯ export INGRESS_PORT="31380"
❯ export INGRESS_HOST="192.168.99.109"
❯ curl -s http://${GATEWAY_URL}/productpage | grep -o "<title>.*</title>"
<title>Simple Bookstore App</title>
```
