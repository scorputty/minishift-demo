#!/usr/bin/env bash

function press (){
  read -n 1 -s -r -p "Press any key to continue"
}

clear

echo "WARNING about to start a complete remove of minishift"
press

echo ""
minishift delete --clear-cache
brew cask remove --force minishift
rm -r ~/.minishift
rm -r ~/.kube

echo "done"
